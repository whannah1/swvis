#!/bin/csh
# Script for using SHDOM optical properties generation system.
# First make each type of scattering table, then make the property file,
# and then run SHDOM.
set AWK=awk   # use nawk on SGI and Sun

set MakeAerosolTable=1       # set to 1 to run the desired section
set MakeWaterTable=0
set MakeIceTables=0
set PlotScatTables=0
set MakeSfc=0
set MakePropertyFile=0
set RunSHDOMsolve=0
set SingleFrames=0
# use perhaps 3 color filters 460 520 and 650
set wavename=065   # 065 for 0.65 um or 164 for 1.64 um wavelength

if ($wavename == 065) then
  # doing this monochromatic
  set wavelen=0.65
endif
if ($wavename == 164) then
  # doing this monochromatic
  set wavelen=1.64
endif

set parfile=loc5t1200.part # this is made by your matlab code
set prpfile=loc5t1200red.prp # this is important large output
set savefile=loc5t1200red.bin # this is important large output
set outfile1=anvil200chop5camera1.pds # this is image output
set outfile2=cldpl.21600_w${wavename}_track1.pds # this "track" doesn't work for 
# some reason but must be left this way

set sfcfile=oceanbrdf01.sfc

# Compile the "put" command
#if (!(-e put))  cc -o put  put.c


  #  Makes the Mie scattering table for spherical water droplets for
  # gamma size distributions with a range of effective radius. 
  set outfile = "water_w${wavename}cgreen.scat"
  set partype = "W"                # W for water
  set distflag=G                   # G=gamma, L=lognormal size distribution
  set alpha = 7                    # gamma dist shape parameter
  set Nretab=1                     # number of effective radius in table
  set Sretab=15; set Eretab=15     # starting, ending effective radius (micron)
  set maxradius=120                 # truncation point for distribution
  set avgflag=C                    # A for averaging over wavelength range
                                   #  C for using central wavelength
  set deltawave=0.003              # wavelength step for averaging (microns)

  if ($avgflag == "A") then
    ./put "$wavelen $wavelen" $partype $avgflag $deltawave $distflag $alpha \
      "$Nretab $Sretab $Eretab" $maxradius $outfile | ./make_mie_table
  else
    echo ./put "$wavelen $wavelen" $partype $avgflag $distflag $alpha \
      "$Nretab $Sretab $Eretab" $maxradius $outfile #| ./make_mie_table
  endif
