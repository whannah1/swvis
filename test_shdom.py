#!/usr/bin/env python
#==============================================================================================
#   This script creates a vizualization of SAM output data  
#   using the SHDOM program developed by Frank Evans    
#   The script does the following: 
#       - make each type of scattering table
#       - make the property file
#       - run SHDOM.
#
#    Aug, 2013  Walter Hannah       Colorado State University
#==============================================================================================
import datetime
import sys
import os
import subprocess as sp
import numpy as np
home = os.getenv("HOME")
#==============================================================================================
#==============================================================================================

SRC_DIR = home+"/SWVIS/SHDOM/"
TOP_DIR = home+"/SWVIS/"

ICE_DB  = SRC_DIR+"sw_ice_scatter.db"

#==============================================================================================
#==============================================================================================
AWK = "awk"   # use nawk on SGI and Sun

MakeAerosolTable = 0    
MakeWaterTable   = 0
MakeIceTables    = 0
PlotScatTables   = 0
MakeSfc          = 0
MakePropertyFile = 0
RunSHDOMsolve    = 1
SingleFrames     = 0

# use perhaps 3 color filters 460 520 and 650
wavename = 065                      # 065 for 0.65 um or 164 for 1.64 um wavelength
color    = "green"                  # set to "" for polychomatic
if wavename == 065 : wavelen=0.65   # monochromatic
if wavename == 164 : wavelen=1.64   # monochromatic

parfile     = "DYNAMO_IDEAL_NSA_DRY_144.part"           # made by a partfile code
prpfile     = "test."+color+".prp"                      # important large output
savefile        = "test."+color+".bin"                  # important large output
outfile1        = "test."+color+".pds"                  # image output

# this "track" doesn't work for some reason but must be left this way
outfile2        = "cldpl.21600_w"+str(wavename)+"_track1.pds" 

sfcfile     = "oceanbrdf01.sfc"

aerscatfile = "./scat/dust_w"    +str(wavename)+".scat"
#watscatfile = "./scat/water_w"   +str(wavename)+"c"+color+".scat"
watscatfile = "water_w"   +str(wavename)+"c"+color+".scat"
icescatfile = "./scat/ice3_150_w"+str(wavename)+color+".scat"

# Compile the "put" command
#CMD = "/usr/bin/cc -o put  "+SRC_DIR+"put.c"
#os.system(CMD)
#==============================================================================================
# Create SCattering Tables
#==============================================================================================
if MakeAerosolTable :
    #  Makes a Mie scattering table for spherical aerosol particles for 
    # lognormal size distributions with a range of effective radius. 
    partype         = "A"                       # A for aerosol
    if wavename == 065 :
        refindex="(1.55,-0.01)"             # an index of refraction for dust aerosol
    if wavename == 164 :
        refindex="(1.55,-1.0E-5)"           # an index of refraction for dust aerosol
    refindex        = "\'"+refindex+"\'"
    pardens     = 2.0                       # particle bulk density (g/cm^3)
    distflag        = "L"                       # G=gamma, L=lognormal size distribution
    sigma       = 0.70                      # lognormal log standard deviation
    Nretab      = 30                        # number of effective radius in table
    Sretab      = 0.1                   
    Eretab      = 3.0                   # starting and ending effective radius (micron)
    maxradius   = 15.0

    CMD = "./put "+str(wavelen)+" "+str(wavelen)+" "+partype    +" \'"+refindex +"\' "+str(pardens)  +" "+distflag+" "+ \
                   str(sigma)  +" "+str(Nretab) +" "+str(Sretab)+" "+str(Eretab)+" "+str(maxradius)+" "+aerscatfile +" | "+SRC_DIR+"make_mie_table"
    print("\n"+CMD+"\n")
    os.system(CMD)

if MakeWaterTable :
    #  Makes the Mie scattering table for spherical water droplets for
    # gamma size distributions with a range of effective radius. 
    partype         = "W"   # W for water
    distflag        = "G"   # G=gamma, L=lognormal size distribution
    alpha       = 7         # gamma dist shape parameter
    Nretab      = 1         # number of effective radius in table
    Sretab      = 15        # starting effective radius (micron)
    Eretab      = 15        # ending effective radius (micron)
    maxradius   = 120       # truncation point for distribution
    avgflag     = "C"       # A for averaging over wavelength range
                            # C for using central wavelength
    deltawave   = 0.003     # wavelength step for averaging (microns)

    if avgflag == "A" :
        CMD = "./put "+str(wavelen)+" "+str(wavelen)+" "+partype    +" "+avgflag    +" "+str(deltawave)  +" "+distflag+" "+ \
                       str(alpha)  +" "+str(Nretab) +" "+str(Sretab)+" "+str(Eretab)+" "+str(maxradius)+"  "+watscatfile +" | "+SRC_DIR+"make_mie_table"
    else :
        CMD = "./put "+str(wavelen)+" "+str(wavelen)+" "+partype    +" "+avgflag    +" "+distflag+" "+ \
                       str(alpha)  +" "+str(Nretab) +" "+str(Sretab)+" "+str(Eretab)+" "+str(maxradius)+" "+watscatfile +" | "+SRC_DIR+"make_mie_table"
    print("\n"+CMD+"\n")
    #exit()
    os.system(CMD)
    exit()

if MakeIceTables :
    #  Makes scattering tables for several ice particle shapes for 
    # gamma size distributions with a range of effective radius. 
    #    Shape index:
    #      1=hollow column, 2=solid column, 3=plate, 4=dendrite
    #      5=rough aggregate, 6=smooth aggregate, 7=bullet-4, 8=bullet-6
    # foreach iceshape (3 5)
    iceshape        = 5
    alpha       = 2             # gamma dist shape parameter
    Nretab      = 121       # number of effective radius in table
    Sretab      = 20            # starting effective radius (micron)
    Eretab      = 150           # ending effective radius (micron)

    CMD = "./put "+ICE_DB+" "+str(wavelen)+" "+str(wavelen)+" "+str(iceshape) +" "+ \
                              str(Nretab) +" "+str(Sretab) +" "+str(Eretab)   +" "+ \
                              str(alpha)+" "+icescatfile +" | "+SRC_DIR+"make_ice_table"
    print("\n"+CMD+"\n")
    os.system(CMD)

#==============================================================================================
# Plot Scattering Tables
#==============================================================================================
if PlotScatTables :
    # Produce plotting files of phase function vs angle with plotscattab
    #CMD = "./put S  dust_w"+str(wavename)+".scat 181 6 \"0.5 1.0 1.5 2.0 2.5 3.0\" dust_w"+str(wavename)+".scatgrf | "+SRC_DIR+"plotscattab"
    CMD = "./put S "+watscatfile+" 361 4 \"5 10 15 20\"     "+watscatfile+"grf | "+SRC_DIR+"plotscattab"
    print("\n"+CMD+"\n")
    os.system(CMD)
    CMD = "./put S  "+icescatfile+"  361 5 \"20 40 60 80 100\" "+icescatfile+"grf  | "+SRC_DIR+"plotscattab"
    print("\n"+CMD+"\n")
    os.system(CMD)
    #CMD = "./put S  ice5_w"+str(wavename)+".scat 361 5 \"20 40 60 80 100\" ice5_w"+str(wavename)+".scatgrf | "+SRC_DIR+"plotscattab"
#==============================================================================================
# Surface
#==============================================================================================
# set OCEAN params
Nxx     = 190
Nyy     = 190
windmin = 5.0
windmax = 10.0
pigment = 0.0
Tsfc        = 300

if MakeSfc :
    CMD1 = AWK+" -v nx="+str(Nxx)+" -v ny="+str(Nyy)+" -v sfc=O \'BEGIN {printf \"%c %3.0f %3.0f %5.2f %5.2f \", sfc, nx,ny,0.1,0.1;}' >! "+sfcfile
    CMD2 = AWK+" -v nx="+str(Nxx)+" -v ny="+str(Nyy)+" -v wmin="+str(windmin)+" -v wmax="+str(windmax)+" -v pig="+str(pigment)+" -v temp="+str(Tsfc)+\
          " \'BEGIN {for (ix=1; ix<=nx; ix++) {for (iy=1; iy<=ny; iy++) {wspd=wmin+(wmax-wmin)*(ix-1)/(nx-1); printf \"%3.0f %3.0f %5.1f %5.2f %5.1f \",ix,iy,temp,wspd,pig;}}}\' >> "+sfcfile
    os.system(CMD1)
    os.system(CMD2)
#==============================================================================================
#==============================================================================================
if MakePropertyFile :
    # Runs propgen to make the SHDOM optical property file for a particle 
    # property file specifying the mass content and effective radius for
    # a mixture of particle types.

    #scattables         = "("+watscatfile+" "+icescatfile+")"
    scattables      = ""+watscatfile+" "+icescatfile+""
    scattypes       = "1 2"
    maxnewphase     = 600
    asymtol         = 0.1
    fracphasetol    = 0.1
    awkcmd          = "awk -v wav="+str(wavelen)+" \'BEGIN {print 2.97E-4*wav^(-4.15+0.2*wav);}\'"
    raylcoef        = sp.Popen(awkcmd, stdout=sp.PIPE, shell=True).communicate()[0].rstrip()
    Nzother         = 2
    #ZTother        = "(0.005 298.7 26 198)"
    ZTother         = "0.005 298.7 26 198"
    CMD = "./put 2 "+str(scattables)  +" "+str(scattypes)+" "+str(parfile)+" "+str(maxnewphase)+" "+str(asymtol)+" "+ \
                     str(fracphasetol)+" "+str(raylcoef) +" "+str(Nzother)+" "+str(ZTother)    +" "+str(prpfile)+"    | ./SHDOM/propgen"
    print
    print CMD
    print
    os.system(CMD)

#exit()

#==============================================================================================
#==============================================================================================

#   Set SHDOM parameters: 
Nmu         = 8                 # number of zenith angles in both hemispheres
Nphi        = 16                # number of azimuth angles
mu0         = 0.94              # solar cosine zenith angle
phi0        = -90.0             # solar beam azimuth (degrees)
flux0       = 0.7               # solar flux (relative)
sfcalb      = 0.1               # surface Lambertian albedo
IPflag      = 0                 # independent pixel flag (0 = 3D, 3 = IP)
BCflag      = 3                 # horizontal boundary conditions (0 = periodic)
deltaM      = "T"               # use delta-M scaling for solar problems
splitacc    = 0.10              # adaptive grid cell splitting accuracy
shacc       = 0.003             # adaptive spherical harmonic accuracy
solacc      = 5.0E-3            # solution accuracy
accel       = "F"               # solution acceleration flag
maxiter     = 60                # maximum number of iterations

if RunSHDOMsolve :
    # Run SHDOM to perform the solution iterations and 
    # store the SHDOM state in a binary save file.
    #Nx = os.system( AWK+" \'BEGIN {getline; getline; print $1;}\' "+str(prpfile) )
    #Ny = os.system( AWK+" \'BEGIN {getline; getline; print $2;}\' "+str(prpfile) )
    #Nz = AWK+" \'BEGIN {getline; getline; print $3;}\' "+str(prpfile) 
    Nx = sp.Popen(AWK+" \'BEGIN {getline; getline; print $1;}\' "+str(prpfile) , stdout=sp.PIPE, shell=True).communicate()[0].rstrip()
    Ny = sp.Popen(AWK+" \'BEGIN {getline; getline; print $2;}\' "+str(prpfile) , stdout=sp.PIPE, shell=True).communicate()[0].rstrip()
    Nz = sp.Popen(AWK+" \'BEGIN {getline; getline; print $3;}\' "+str(prpfile) , stdout=sp.PIPE, shell=True).communicate()[0].rstrip()

    CMD = "./put Visualize "+str(prpfile)+" NONE NONE NONE "+str(savefile)      \
         +" "+str(Nx)+" "+str(Ny)+" "+str(Nz)+" "+str(Nmu)+" "+str(Nphi)        \
         +" "+str(BCflag)+" "+str(IPflag)+" "+deltaM+" P S "+str(flux0)         \
         +" "+str(mu0)+" "+str(phi0)+" 0.0 "+str(sfcalb)+" "+str(wavelen)       \
         +" "+str(splitacc)+" "+str(shacc)+"  "+accel+" "+str(solacc)+" "+str(maxiter)+" 0 NONE 9216 1.1 0.4 1 | ./SHDOM/shdom90"
    print CMD
    os.system(CMD)

    # if not using a $sfcfile put $sfcalb right before $wavelen

#if SingleFrames :
    #  Make single images for camera and cross track scanning modes.
    # An SHDOM save file is input.
    # V   1, nbytes, scale, X,Y,Z, theta, phi, rotang, NL, NS, delline, delsamp
    # V   2, nbytes, scale, X1,Y1,Z1, X2,Y2,Z2, spacing, scan1, scan2, delscan
    # theta=0=looking up, theta=180=looking down, phi is left and right
    # to increase image resolution, increase NL, NS, leave delline, delsamp alone
    # but make sure you update pdstojpeg.pro with the new info if going to jpeg
    # set outfile1=cldpl.21600_w${wavename}_camera1.pds
    # set outfile2=cldpl.21600_w${wavename}_track1.pds
    
#   maxiter = 0           
#   Nx=`$AWK 'BEGIN {getline; getline; print $1;}' $prpfile`
#   Ny=`$AWK 'BEGIN {getline; getline; print $2;}' $prpfile`
#   Nz=`$AWK 'BEGIN {getline; getline; print $3;}' $prpfile`
#
#   ./put Visualize $prpfile NONE NONE $savefile NONE "$Nx $Ny $Nz" "$Nmu $Nphi"\
#     $BCflag $IPflag $deltaM P S "$flux0 $mu0 $phi0" 0.0 $sfcalb $wavelen \
#     "$splitacc $shacc" "$accel $solacc $maxiter" \
#   2   V "1 1 900 15.1 48.9 13.0 113 235 0  600 800 0.10 0.10" $outfile1 \
#       V "2 1 800 0.1 10.0 18.  20.0 10.0 18. 0.30 -30 30 0.15" $outfile2 \
#    NONE 5200 1.1 0.4 1 | ./shdom90


#==============================================================================================
#==============================================================================================
