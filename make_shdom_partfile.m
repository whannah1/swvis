%% Variable Dictionary:

%declare large variables
mnx=2048; mny=2048;
top=209;
tab=zeros(mnx,mny);

qn=zeros(mnx,mny);

qp=zeros(mnx,mny);

%initialize fields
nxb=190;nyb=190;
qns=zeros(nxb,nyb);
ts=zeros(nxb,nyb);
qi=zeros(nxb,nyb,top);
qc=zeros(nxb,nyb,top);
qs=zeros(nxb,nyb,top);
% Constants used by SAM
cp = 1004.0; % Specific heat of air, J/kg/K
ggr = 9.81; % Gravity acceleration, m/s2
lcond = 2.5104e06; % Latent heat of condensation

% choose effective radius measure (microns)
re_liq=15;
re_ice=30;
re_snow=150; % snow effective radius


%%% Open 3D time dependent binary files 

%%% Do you want to do 5 min times from 4-8 hours? Then set times=[0:48] where
% t=0 is time 4:00, t=1 is 4:05, etc, t=48 is time 8:00
% t=28 is time 6:20 the first main "catscan" time

% Do you want to do 1 hour times from 12-24 hours? Then set times=[96:12:240]

% times=0:48;
% times=96:12:240;
times=96; % 240 is currently the only time with u,v data besides 5:7hrs
% times=[30:33,32:-1:28];
disp(times)

nt=length(times);
inttimes=7200+(times*150);

% for it=1:nt
    
t=times;
    
intvar=inttimes;
varname=sprintf('%05i',intvar);
disp(varname);

% Time stamp
% tot=intvar/1800;
% thrs=floor(tot); % integer
% tmins=nearest((tot-thrs)*60); % integer
% hrs=sprintf('%02u',thrs); % string
% mins=sprintf('%02u',tmins); % string
% disp([hrs ':' mins]);

if (t>48 && t<96)
    error('Cannot use times between 48 - 96 (8:00 and 12:00 hours)');
end

% do u, v, or pp
% if (t>=12 && t<=36)
%     fid7=fopen(['/uufs/chpc.utah.edu/common/home/krueger-group/skrueger/GATE_IDEAL_S_2048x2048x256_100m_2s/binary_files/GATE_IDEAL_2048x2048_u_00000' (varname) '.dat']);
%     fid8=fopen(['/uufs/chpc.utah.edu/common/home/krueger-group/skrueger/GATE_IDEAL_S_2048x2048x256_100m_2s/binary_files/GATE_IDEAL_2048x2048_v_00000' (varname) '.dat']);
% end

% if t==240
%     fid7=fopen(['/uufs/chpc.utah.edu/common/home/krueger_grp/pbogen/sam/GATE_IDEAL_2048/binary_files/GATE_IDEAL_2048x2048_u_00000' (varname) '.dat']);
%     fid8=fopen(['/uufs/chpc.utah.edu/common/home/krueger_grp/pbogen/sam/GATE_IDEAL_2048/binary_files/GATE_IDEAL_2048x2048_v_00000' (varname) '.dat']);
% end

    
    % Declare fid's

    fid1=fopen(['/uufs/chpc.utah.edu/common/home/krueger_data1/skrueger/GATE_IDEAL_S_2048x2048x256_100m_2s/binary_files/GATE_IDEAL_2048x2048_tab_00000' (varname) '.dat']);
%     fid2=fopen(['/uufs/chpc.utah.edu/common/home/krueger_data1/skrueger/GATE_IDEAL_S_2048x2048x256_100m_2s/binary_files/GATE_IDEAL_2048x2048_qv_00000' (varname) '.dat']);
%     fid3=fopen(['/uufs/chpc.utah.edu/common/home/krueger_data1/skrueger/GATE_IDEAL_S_2048x2048x256_100m_2s/binary_files/GATE_IDEAL_2048x2048_w_00000' (varname) '.dat']);
    fid35=fopen(['/uufs/chpc.utah.edu/common/home/krueger_data1/skrueger/GATE_IDEAL_S_2048x2048x256_100m_2s/binary_files/GATE_IDEAL_2048x2048_qn_00000' (varname) '.dat']);
    fid37=fopen(['/uufs/chpc.utah.edu/common/home/krueger_data1/skrueger/GATE_IDEAL_S_2048x2048x256_100m_2s/binary_files/GATE_IDEAL_2048x2048_qp_00000' (varname) '.dat']);
%     fid6=fopen(['/uufs/chpc.utah.edu/common/home/krueger-group/skrueger/GATE_IDEAL_S_2048x2048x256_100m_2s/binary_files/GATE_IDEAL_2048x2048_pp_00000' (varname) '.dat']);

fid4=fopen('/uufs/chpc.utah.edu/common/home/krueger_grp/pbogen/sam/GATE_IDEAL_2048/OUT_2D/z.dat');
fid5=fopen('/uufs/chpc.utah.edu/common/home/krueger_grp/pbogen/sam/GATE_IDEAL_2048/OUT_2D/rho.dat');
fid6=fopen('/uufs/chpc.utah.edu/common/home/krueger_grp/pbogen/sam/GATE_IDEAL_2048/OUT_2D/tabs.dat');
fid7=fopen('/uufs/chpc.utah.edu/common/home/krueger_grp/pbogen/sam/GATE_IDEAL_2048/OUT_2D/p.dat');


z_tot=fread(fid4,256,'float32'); % meters, z for each lvl kk
z=z_tot(1:top);

rho=fread(fid5,256,'float32'); % kg/m3, rho for each lvl kk

tbars=fread(fid6,[256,290],'float32'); % K, avg T at each lvl, time
% Note time=21600, in this simulation time=1 actually equals 2 seconds
% so time=21600 = 43200 seconds = 12 hours, so that's 12/24 hours through
% the 290 which is 145 since 145/290=12/24
tbar=tbars(:,145);

pbar=fread(fid7,256,'float32'); % hPa, avg p at each lvl kk


nomout='loc5t1200.part';
% if (exist(nomout,'file') == 0)

% loop over levels
for kk=1:top
disp(kk);

% w=1.0e3*fread(fid3,[nx,ny],'float32','ieee-be');% mm/s
% wplus=1.0e3*fread(fid3,[nx,ny],'float32','ieee-be');% mm/s
% w=w*1.0e3; % m/s
% wplus=wplus*1.0e3; % m/s
% 
% wavg=(w+wplus)/2.0;
% clear w; clear wplus;
%disp('w done');

qn=fread(fid35,[mnx,mny],'float32','ieee-be');% g/kg
tab=fread(fid1,[mnx,mny],'float32','ieee-be');% K
qp=fread(fid37,[mnx,mny],'float32','ieee-be');% g/kg


% Subsection of cool cold pool looking part (1201:1800,201:800) (600x600)
% Or try a smaller part (1601:1800,201:400) (200x200) original, expanded to
% 256x256 best by going back in rows but forward in cols
% (1544:1800,201:456) or something like that

% what ever the nxb,nyb is here needs to be nxb, nyb above
% qns(:,:)=qn(1601:1800,201:400);
% ts(:,:)=tab(1601:1800,201:400);
% qps(:,:)=qp(1601:1800,201:400);
% from doing some tests, I think it would be good to cut down the
% rhs to 500 and increase the top by some, maybe 30 or 50 so I'm talking
% about going to: qns=qn(1201:1830,201:700) of course that complicates the
% sz averging down but we'll need to work on that
% switch to below 300+2 format for surrounding with zeros
% let's try 190x190
qns(:,:)=qn(1690:1879,222:411);
ts(:,:)=tab(1690:1879,222:411);
qps(:,:)=qp(1690:1879,222:411);

clear qn tab qp

% In SAM, qc (liq.) and qi (ice) are fractions of qn based on a temperature
% function wn
t1n=273.16; % K
t2n=253.16; % K
t1p=283.16;
t2p=268.16;
t1g=283.16;
t2g=223.16;
%wn=max(0,min(1,(tab-t2n)/(t1n-t2n)));
qi(:,:,kk)=(1-max(0,min(1,(ts-t2n)/(t1n-t2n)))).*qns;
qc(:,:,kk)=(max(0,min(1,(ts-t2n)/(t1n-t2n)))).*qns;
% qr(:,:,kk)=(max(0,min(1,(ts-t2p)/(t1p-t2p)))).*qps;
qs(:,:,kk)=(1-(max(0,min(1,(ts-t2p)/(t1p-t2p))))).*(1-(max(0,min(1,(ts-t2g)/(t1g-t2g))))).*qps;
end % lvls

%%%%%%% Using Qiang Fu gfu@atmos.washington.edu method to combine ice and
%%%%%%% snow

erra=1/((qs./(qs+qi))*(1/re_snow) + (qi./(qs+qi))*(1/re_ice));

erra(isnan(erra))=0;
%% snow
qi=qi+qs;

clear re_ice
re_ice=erra;
clear erra


%% here is where the code really begins
nx=nxb;ny=nyb;

nz=length(z);
iz1=1;iz2=nz;
disp(['levels from ' num2str(iz1) '=' num2str(z(iz1)) ' to ' num2str(iz2) '=' num2str(z(iz2))]);

% zc is cloudy z %%% could rework this part if large section non-cloudy
zc=z(iz1:iz2);
nz=length(zc)+1;
zlev(2:nz)=zc;
zlev(1)=10;          % add level 0 for visualization
        
% Keep only q > 0 for z 
qi2(:,:,2:nz)=qi(:,:,iz1:iz2); 
qi2(:,:,1)=zeros(nx,ny,1);
re_ice2(:,:,2:nz)=re_ice(:,:,iz1:iz2); 
re_ice2(:,:,1)=zeros(nx,ny,1);
clear re_ice
        
qc2(:,:,2:nz)=qc(:,:,iz1:iz2); 
qc2(:,:,1)=zeros(nx,ny,1);

% qr2(:,:,2:nz)=qs(:,:,iz1:iz2); 
% qr2(:,:,1)=zeros(nx,ny,1);

%%%%% OPTICAL DEPTH REDUCTION CENTER: LESS LIQUID %%%%%
% all this lwc is sometimes more than necessary for visualization 
%qc2(qc2>1)=1;
%qi2(qi2>1)=1;

% % comment out below if you are going to use the file size reduction center
% % and vice versa, this lets you use all vertical levels
iwc=zeros(nx,ny,nz);lwc=zeros(nx,ny,nz);
for levy=1:nz
    iwc(:,:,levy)=qi2(:,:,levy)*rho(levy); % units g/m^3
    lwc(:,:,levy)=qc2(:,:,levy)*rho(levy); % units g/m^3
end
re_ice=re_ice2;
zlev2=zlev;
tlev2(1)=298;tlev2(2:nz)=tbar(2:nz);
nz2=nz;

%%%%% FILE SIZE REDUCTION CENTER: EVERYTHING HALF OFF! %%%%%%

% To reduce file size, reduce the vertical resolution by only using every
% other z level after the 1st vis level, that is, keep levels 1,2,4,6 etc.

% nz2=floor(nz/2)+1;
% iwc=zeros(nx,ny,nz2);lwc=zeros(nx,ny,nz2);zlev2=zeros(nz2,1);
% tlev2=zeros(nz2,1);tlev2(1)=tbar(1);
% 
% for ii=2:2:nz
%    iwc(:,:,(ii/2)+1)=qi2(:,:,ii)*rho(ii);
%    lwc(:,:,(ii/2)+1)=qc2(:,:,ii)*rho(ii);
%    zlev2((ii/2)+1)=zlev(ii);
%    tlev2((ii/2)+1)=tbar(ii);
% end


zlev2=zlev2/1000; % km untis


% Now output in the proper .part format style, detailed in the middle of
%% the page http://nit.colorado.edu/shdom/shdomdoc/index.html
dx=0.1;dy=dx; % km, specific to GigaLES
% fluff lwc, iwc, re_ice

% make edges zero for doing closed boundary conditions
lwc(1,:,:)=0;lwc(end,:,:)=0;lwc(:,1,:)=0;lwc(:,end,:)=0;
iwc(1,:,:)=0;iwc(end,:,:)=0;iwc(:,1,:)=0;iwc(:,end,:)=0;
re_ice(1,:,:)=0;re_ice(end,:,:)=0;re_ice(:,1,:)=0;re_ice(:,end,:)=0;

iwcf=2;
disp([' write file' nomout]);

fidout=fopen(nomout,'w');
fprintf(fidout,'%i\n',3);
fprintf(fidout,'%i %i %i\n',nxb,nyb,nz2);
fprintf(fidout,'%5.3f ',dx,dy);
fprintf(fidout,'\n');

fprintf(fidout,'%7.5f ',zlev2(:));
fprintf(fidout,'\n');

fprintf(fidout,'%7.4f ',tlev2(:));
fprintf(fidout,'\n');

% old way
% for ix=1:nx
%     for iy=1:ny
%         for iz=1:nz2
%             fprintf(fidout,'%i %i %i %i %i %3.6f %2.2f %i %3.6f %2.2f ',ix,iy,iz,2,1,lwc(ix,iy,iz),Re_liq,2,iwc(ix,iy,iz),Re_ice);
%             fprintf(fidout,'\n');
%         end
%     end
% end

% new way for 2 particle types

disp('range of ice eff_radius')
disp(max(max(max(re_ice))))
disp(min(min(min(re_ice))))
re_ice=round(re_ice);
re_ice(re_ice<15)=15;

for ix=1:ny
    disp(ix)
    for iy=1:ny
        for iz=1:nz2
            % consider all possible combos
            if (iwc(ix,iy,iz)~=0) && lwc(ix,iy,iz)~=0
            fprintf(fidout,'%i %i %i %i %i %2.2E %2.2f %i %2.2E %2.2f ',ix,iy,iz,2,1,lwc(ix,iy,iz),re_liq,2,iwc(ix,iy,iz),re_ice(ix,iy,iz));
            fprintf(fidout,'\n');
            end
            if iwc(ix,iy,iz)~=0 && lwc(ix,iy,iz)==0
            fprintf(fidout,'%i %i %i %i %i %2.2E %2.2f',ix,iy,iz,1,2,iwc(ix,iy,iz),re_ice(ix,iy,iz));
            fprintf(fidout,'\n');
            end
            if iwc(ix,iy,iz)==0 && lwc(ix,iy,iz)~=0
            fprintf(fidout,'%i %i %i %i %i %2.2E %2.2f',ix,iy,iz,1,1,lwc(ix,iy,iz),re_liq);
            fprintf(fidout,'\n');
            end 
%             comment out the below to try not even writing grid points
%             with zero mass
%             if lwc(ix,iy,iz)==0 && iwc(ix,iy,iz)==0
%             fprintf(fidout,'%i %i %i %i %i %2.2E %2.2f',ix,iy,iz,1,1,0,re_liq);
%             fprintf(fidout,'\n');
%             end               
        end
    end
end


% % new way for 3 particle types
% for ix=1:nx
%     for iy=1:ny
%         for iz=1:nz2
%             % consider all possible combos
%             if ((rwc(ix,iy,iz)~=0 && iwc(ix,iy,iz)~=0) && lwc(ix,iy,iz)~=0)
%             fprintf(fidout,'%i %i %i %i %i %3.6f %2.2f %i %3.6f %2.2f %i %3.6f %2.2f ',ix,iy,iz,3,1,lwc(ix,iy,iz),re_liq,2,iwc(ix,iy,iz),re_ice,3,rwc(ix,iy,iz),re_snow);
%             fprintf(fidout,'\n');
%             end
%             if ((rwc(ix,iy,iz)~=0 && iwc(ix,iy,iz)~=0) && lwc(ix,iy,iz)==0)
%             fprintf(fidout,'%i %i %i %i %i %3.6f %2.2f %i %3.6f %2.2f ',ix,iy,iz,2,2,iwc(ix,iy,iz),re_ice,3,rwc(ix,iy,iz),re_snow);
%             fprintf(fidout,'\n');
%             end
%             if ((rwc(ix,iy,iz)~=0 && iwc(ix,iy,iz)==0) && lwc(ix,iy,iz)~=0)
%             fprintf(fidout,'%i %i %i %i %i %3.6f %2.2f %i %3.6f %2.2f ',ix,iy,iz,2,1,lwc(ix,iy,iz),re_liq,3,rwc(ix,iy,iz),re_snow);
%             fprintf(fidout,'\n');
%             end
%             if ((rwc(ix,iy,iz)==0 && iwc(ix,iy,iz)~=0) && lwc(ix,iy,iz)~=0)
%             fprintf(fidout,'%i %i %i %i %i %3.6f %2.2f %i %3.6f %2.2f ',ix,iy,iz,2,1,lwc(ix,iy,iz),re_liq,2,iwc(ix,iy,iz),re_ice);
%             fprintf(fidout,'\n');
%             end
%             if ((rwc(ix,iy,iz)==0 && iwc(ix,iy,iz)==0) && lwc(ix,iy,iz)~=0)
%             fprintf(fidout,'%i %i %i %i %i %3.6f %2.2f ',ix,iy,iz,1,1,lwc(ix,iy,iz),re_liq);
%             fprintf(fidout,'\n');
%             end
%             if ((lwc(ix,iy,iz)==0 && iwc(ix,iy,iz)~=0) && rwc(ix,iy,iz)==0)
%             fprintf(fidout,'%i %i %i %i %i %3.6f %2.2f ',ix,iy,iz,1,2,iwc(ix,iy,iz),re_ice);
%             fprintf(fidout,'\n');
%             end
%             if ((lwc(ix,iy,iz)==0 && iwc(ix,iy,iz)==0) && rwc(ix,iy,iz)~=0)
%             fprintf(fidout,'%i %i %i %i %i %3.6f %2.2f ',ix,iy,iz,1,3,rwc(ix,iy,iz),re_snow);
%             fprintf(fidout,'\n');
%             end    
%             if ((lwc(ix,iy,iz)==0 && iwc(ix,iy,iz)==0) && rwc(ix,iy,iz)==0)
%             fprintf(fidout,'%i %i %i %i ',ix,iy,iz,0);
%             fprintf(fidout,'\n');
%             end               
%         end
%     end
% end

fclose(fidout);
disp(['fclose', nomout])

% else
% disp([nomout,' already created']);
% end
