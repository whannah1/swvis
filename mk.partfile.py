#!/usr/bin/env python
#========================================================================================================================
# 	This script creates a "partfile" from SAM output data
#   suitable to create a vizualization  with SHDOM
# 	The script does the following: 
#		- ?
#
#    Sept, 2013	Walter Hannah 		Colorado State University
#========================================================================================================================
import datetime
import sys
import os
import math
import numpy as np
from scipy.io import netcdf as nc
np.seterr(divide='ignore')
np.seterr(invalid='ignore')
#========================================================================================================================
#========================================================================================================================

ifile = "DYNAMO_IDEAL_NSA_DRY_500_144x64_00_36_0000034560.nc"
ofile = "DYNAMO_IDEAL_NSA_DRY_144.part"

#declare large variables
mnx = 144 
mny = 144
top = 64

#initialize fields
nxb = 144
nyb = 144

# Constants used by SAM
cp 		= 1004.0 		# Specific heat of air, J/kg/K
ggr 		= 9.81			# Gravity acceleration, m/s2
lcond 	= 2.5104e06 		# Latent heat of condensation

# choose effective radius measure (microns)
re_liq  = 15
re_ice  = 30
re_snow = 150 # snow effective radius
#========================================================================================================================
# Define time
#========================================================================================================================
# Do you want to do 5 min times from 4-8 hours? Then set times=[0:48] where
# t=0 is time 4:00, t=1 is 4:05, etc, t=48 is time 8:00
# t=28 is time 6:20 the first main "catscan" time
# Do you want to do 1 hour times from 12-24 hours? Then set times=[96:12:240]
# times=0:48
# times=96:12:240

times = 0

nt 		 = 1
inttimes = 7200+(times*150)

t = times
    
intvar	= inttimes
#varname	= sprintf('%05i',intvar)
#========================================================================================================================    
# Open input files
#========================================================================================================================
ifile = nc.netcdf_file(ifile,"r")

z   = ifile.variables["z"].data   
p   = ifile.variables["p"].data   
TA  = ifile.variables["TABS"].data[0,:,:,:]
QN  = ifile.variables["QNL"].data [0,:,:,:]
QNI = ifile.variables["QNI"].data [0,:,:,:]
QPI = ifile.variables["QPI"].data [0,:,:,:]
P   = p.reshape(64,1,1) + ifile.variables["PP"].data[0,:,:,:]

ifile.close

rho = P/(287.*TA)

tbar = np.average(TA.reshape(top,-1),1)
pbar = np.average( P.reshape(top,-1),1)
#========================================================================================================================
#  Use Qiang Fu  method to combine ice and snow (gfu@atmos.washington.edu)
#========================================================================================================================

QI = QNI + QPI

erra = 1./( QPI/(QI*re_snow) + QNI/(QI*re_ice) )

erra = np.where(np.isnan(erra),0.,erra)


QS = erra
#========================================================================================================================
# Here is where the code really begins
#========================================================================================================================
nx=nxb
ny=nyb

nz  = len(z)
iz1 = 1
iz2 = nz

# zc is cloudy z
zc = z[iz1:iz2]
nz = len(zc)+1
zlev = np.zeros(nz)
zlev[1:nz]= zc
zlev[0]   = 10          # add level 0 for visualization
        
# Keep only q > 0 for z 
QI2 = np.zeros([nz,ny,nx])
QN2 = np.zeros([nz,ny,nx])
QS2 = np.zeros([nz,ny,nx])
QI2[1:nz,:,:] = QI[iz1:iz2,:,:]
QN2[1:nz,:,:] = QN[iz1:iz2,:,:]
QS2[1:nz,:,:] = QS[iz1:iz2,:,:]
#========================================================================================================================
# %%%%% OPTICAL DEPTH REDUCTION CENTER: LESS LIQUID %%%%%
#========================================================================================================================
# all this LWC is sometimes more than necessary for visualization 

IWC = QI2*rho # units g/m^3
LWC = QN2*rho # units g/m^3
QS 	 	 	= QS2
zlev2	 	= zlev
tlev2	 	= zlev
tlev2[1] 	= 298
tlev2[2:nz]	= tbar[2:nz]

zlev2		= zlev2/1000 # convert to km 

# Now output in the proper .part format style, detailed in the middle of
# the page http://nit.colorado.edu/shdom/shdomdoc/index.html
dx = 0.2
dy = dx

# make edges zero for doing closed boundary conditions
LWC[:, 1,:]   	= 0
LWC[:,-1,:]  	= 0
LWC[:,:, 1]    	= 0
LWC[:,:,-1]  	= 0

IWC[:, 1,:]    	= 0
IWC[:,-1,:]  	= 0
IWC[:,:, 1]		= 0
IWC[:,:,-1] 		= 0

QS[:, 1,:] 		= 0
QS[:,-1,:]		= 0
QS[:,:, 1]		= 0
QS[:,:,-1]		= 0

IWCf = 2

#========================================================================================================================
# Write output file
#========================================================================================================================
outfile = open(ofile, 'w')

# Write file header 
outfile.write("3\n")

outfile.write(str(nxb)+"  "+str(nyb)+"  "+str(nz))
outfile.write("\n")

outfile.write("%5.3f %5.3f" % ( dx,dy ) )
outfile.write("\n")

outfile.write(' '.join('%7.5f'%F for F in zlev2 ) )
outfile.write("\n")

outfile.write(' '.join('%7.4f'%F for F in tlev2 ) )
outfile.write("\n")

#fprintf(fidout,'%i\n',3)
#fprintf(fidout,'%i %i %i\n',nxb,nyb,nz2)
#fprintf(fidout,'%5.3f ',dx,dy)
#fprintf(fidout,'\n')

#fprintf(fidout,'%7.5f ',zlev2(:))
#fprintf(fidout,'\n')

#fprintf(fidout,'%7.4f ',tlev2(:))
#fprintf(fidout,'\n')


# new way for 2 particle types
#disp('range of ice eff_radius')
#disp(max(max(max(re_ice))))
#disp(min(min(min(re_ice))))
QS 			= np.around(QS)
#QS(QS<15.) 	= 15.
QS = np.where(QS<15.,15.,QS)
 
for ix in range(0,ny):
	for iy in range(0,ny):
		for iz in range(0,nz):
			# consider all possible combos
			if (IWC[iz,iy,ix]==0) and (LWC[iz,iy,ix]==0) :
				outfile.write( "%i %i %i %i %i %2.2E %2.2f %i %2.2E %2.2f " % ( ix,iy,iz,2,1,LWC[iz,iy,ix],re_liq,2,IWC[iz,iy,ix],QS[iz,iy,ix] ) )
				outfile.write("\n")
			if IWC[iz,iy,ix]!=0 and LWC[iz,iy,ix]==0 :
				outfile.write( "%i %i %i %i %i %2.2E %2.2f " % ( ix,iy,iz,1,2,IWC[iz,iy,ix],QS[iz,iy,ix] ) )
				outfile.write("\n")
			if IWC[iz,iy,ix]==0 and LWC[iz,iy,ix]!=0 :
				outfile.write( "%i %i %i %i %i %2.2E %2.2f " % ( ix,iy,iz,1,1,LWC[iz,iy,ix],re_liq ) )
				outfile.write("\n")


outfile.close()

print
print "  "+ofile
print

