#!/bin/csh
# Script for using SHDOM optical properties generation system.
# First make each type of scattering table, then make the property file,
# and then run SHDOM.
set AWK=awk   # use nawk on SGI and Sun


set MakeAerosolTable=0       # set to 1 to run the desired section
set MakeWaterTable=0
set MakeIceTables=0
set PlotScatTables=0
set MakeSfc=0
set MakePropertyFile=0
set RunSHDOMsolve=0
set SingleFrames=0
# use perhaps 3 color filters 460 520 and 650
set wavename=065   # 065 for 0.65 um or 164 for 1.64 um wavelength

if ($wavename == 065) then
  # doing this monochromatic
  set wavelen=0.65
endif
if ($wavename == 164) then
  # doing this monochromatic
  set wavelen=1.64
endif

set parfile=loc5t1200.part # this is made by your matlab code
set prpfile=loc5t1200red.prp # this is important large output
set savefile=loc5t1200red.bin # this is important large output
set outfile1=anvil200chop5camera1.pds # this is image output
set outfile2=cldpl.21600_w${wavename}_track1.pds # this "track" doesn't work for 
# some reason but must be left this way

set sfcfile=oceanbrdf01.sfc

# Compile the "put" command
if (!(-e put))  cc -o put  put.c


if ($MakeAerosolTable) then
  #  Makes a Mie scattering table for spherical aerosol particles for 
  # lognormal size distributions with a range of effective radius. 
  set outfile = "dust_w${wavename}.scat"
  set partype = "A"               # A for aerosol
  if ($wavename == 065) then
    set refindex="(1.55,-0.01)"    # an index of refraction for dust aerosol
  endif
  if ($wavename == 164) then
    set refindex="(1.55,-1.0E-5)"  # an index of refraction for dust aerosol
  endif
  set pardens=2.0                  # particle bulk density (g/cm^3)
  set distflag=L                   # G=gamma, L=lognormal size distribution
  set sigma = 0.70                 # lognormal log standard deviation
  set Nretab=30                    # number of effective radius in table
  set Sretab=0.1; set Eretab=3.0   # starting and ending effective radius (micron)
  set maxradius=15.0

  ./put "$wavelen1 $wavelen2" $partype "$refindex" $pardens $distflag $sigma \
      "$Nretab $Sretab $Eretab" $maxradius $outfile | ./make_mie_table
endif



if ($MakeWaterTable) then
  #  Makes the Mie scattering table for spherical water droplets for
  # gamma size distributions with a range of effective radius. 
  set outfile = "water_w${wavename}cgreen.scat"
  set partype = "W"                # W for water
  set distflag=G                   # G=gamma, L=lognormal size distribution
  set alpha = 7                    # gamma dist shape parameter
  set Nretab=1                     # number of effective radius in table
  set Sretab=15; set Eretab=15     # starting, ending effective radius (micron)
  set maxradius=120                 # truncation point for distribution
  set avgflag=C                    # A for averaging over wavelength range
                                   #  C for using central wavelength
  set deltawave=0.003              # wavelength step for averaging (microns)

  if ($avgflag == "A") then
    ./put "$wavelen $wavelen" $partype $avgflag $deltawave $distflag $alpha \
      "$Nretab $Sretab $Eretab" $maxradius $outfile | ./make_mie_table
  else
    ./put "$wavelen $wavelen" $partype $avgflag $distflag $alpha \
      "$Nretab $Sretab $Eretab" $maxradius $outfile | ./make_mie_table
  endif
endif



if ($MakeIceTables) then
  #  Makes scattering tables for several ice particle shapes for 
  # gamma size distributions with a range of effective radius. 
  #    Shape index:
  #      1=hollow column, 2=solid column, 3=plate, 4=dendrite
  #      5=rough aggregate, 6=smooth aggregate, 7=bullet-4, 8=bullet-6
  #
  # foreach iceshape (3 5)
    set iceshape=5
    set outfile = "icesnowcomb150green.scat"
    set alpha = 2                  # gamma dist shape parameter
    set Nretab=121                   # number of effective radius in table
    set Sretab=30; set Eretab=150   # starting and ending effective radius (micron)

    ./put sw_ice_scatter.db "$wavelen $wavelen" $iceshape \
      "$Nretab $Sretab $Eretab" $alpha $outfile | ./make_ice_table
  #end
endif



if ($PlotScatTables) then
  # Produce plotting files of phase function vs angle with plotscattab
   
   #put S dust_w${wavename}.scat 181 6 "0.5 1.0 1.5 2.0 2.5 3.0" dust_w${wavename}.scatgrf | plotscattab 
   ./put S water_w${wavename}c.scat 361 4 "5 10 15 20" water_w${wavename}c.scatgrf | plotscattab 
   ./put S ice3_w${wavename}.scat 361 5 "20 40 60 80 100" ice3_w${wavename}.scatgrf | plotscattab 
   #put S ice5_w${wavename}.scat 361 5 "20 40 60 80 100" ice5_w${wavename}.scatgrf | plotscattab 
endif


#set OCEAN params
set Nxx=190
set Nyy=190
set windmin=5.0
set windmax=10.0
set pigment=0.0
set Tsfc=300

if ($MakeSfc) then
$AWK -v nx=$Nxx -v ny=$Nyy -v sfc=O 'BEGIN {printf "%c\n%3.0f %3.0f %5.2f %5.2f\n", sfc, nx,ny,0.1,0.1;}' >! $sfcfile
$AWK -v nx=$Nxx -v ny=$Nyy -v wmin=$windmin -v wmax=$windmax -v pig=$pigment -v temp=$Tsfc 'BEGIN {for (ix=1; ix<=nx; ix++) {for (iy=1; iy<=ny; iy++) {wspd=wmin+(wmax-wmin)*(ix-1)/(nx-1); printf "%3.0f %3.0f %5.1f %5.2f %5.1f\n",ix,iy,temp,wspd,pig;}}}' >> $sfcfile
endif

if ($MakePropertyFile) then
  # Runs propgen to make the SHDOM optical property file for a particle 
  # property file specifying the mass content and effective radius for
  # a mixture of particle types.


  set scattables = (water_w${wavename}c.scat icesnow1_150red.scat)
  set scattypes = (1 2)
  set maxnewphase=600
  set asymtol=0.1
  set fracphasetol=0.1
  set raylcoef=`awk -v wav=$wavelen 'BEGIN {print 2.97E-4*wav^(-4.15+0.2*wav);}'` 
  set Nzother=2
  set ZTother=(0.005 298.7 26 198)

  ./put 2 $scattables "$scattypes" $parfile \
      $maxnewphase $asymtol $fracphasetol $raylcoef \
      $Nzother $ZTother $prpfile  | ./propgen
endif




#   Set SHDOM parameters: 
set Nmu=8                 # number of zenith angles in both hemispheres
set Nphi=16               # number of azimuth angles
set mu0=0.94              # solar cosine zenith angle
set phi0=-90.0              # solar beam azimuth (degrees)
set flux0=0.7             # solar flux (relative)
set sfcalb=0.1           # surface Lambertian albedo
set IPflag=0              # independent pixel flag (0 = 3D, 3 = IP)
set BCflag=3              # horizontal boundary conditions (0 = periodic)
set deltaM=T              # use delta-M scaling for solar problems
set splitacc=0.10         # adaptive grid cell splitting accuracy
set shacc=0.003           # adaptive spherical harmonic accuracy
set solacc=5.0E-3         # solution accuracy
set accel=F               # solution acceleration flag
set maxiter=60           # maximum number of iterations



if ($RunSHDOMsolve) then
  # Run SHDOM to perform the solution iterations and store the SHDOM
  #  state in a binary save file.

set Nx=`$AWK 'BEGIN {getline; getline; print $1;}' $prpfile`
set Ny=`$AWK 'BEGIN {getline; getline; print $2;}' $prpfile`
set Nz=`$AWK 'BEGIN {getline; getline; print $3;}' $prpfile`

  ./put Visualize $prpfile NONE NONE NONE $savefile "$Nx $Ny $Nz" "$Nmu $Nphi"\
      $BCflag $IPflag $deltaM P S "$flux0 $mu0 $phi0" 0.0 $sfcalb $wavelen \
      "$splitacc $shacc" "$accel $solacc $maxiter" 0 \
      NONE 9216 1.1 0.4 1 | ./shdom90

# if not using a $sfcfile put $sfcalb right before $wavelen
endif



if ($SingleFrames) then
  #  Make single images for camera and cross track scanning modes.
  # An SHDOM save file is input.
  # V   1, nbytes, scale, X,Y,Z, theta, phi, rotang, NL, NS, delline, delsamp
  # V   2, nbytes, scale, X1,Y1,Z1, X2,Y2,Z2, spacing, scan1, scan2, delscan
  # theta=0=looking up, theta=180=looking down, phi is left and right
  # to increase image resolution, increase NL, NS, leave delline, delsamp alone
  # but make sure you update pdstojpeg.pro with the new info if going to jpeg

  # set outfile1=cldpl.21600_w${wavename}_camera1.pds
  # set outfile2=cldpl.21600_w${wavename}_track1.pds
  set maxiter=0           
set Nx=`$AWK 'BEGIN {getline; getline; print $1;}' $prpfile`
set Ny=`$AWK 'BEGIN {getline; getline; print $2;}' $prpfile`
set Nz=`$AWK 'BEGIN {getline; getline; print $3;}' $prpfile`

  ./put Visualize $prpfile NONE NONE $savefile NONE "$Nx $Ny $Nz" "$Nmu $Nphi"\
      $BCflag $IPflag $deltaM P S "$flux0 $mu0 $phi0" 0.0 $sfcalb $wavelen \
      "$splitacc $shacc" "$accel $solacc $maxiter" \
    2   V "1 1 900 15.1 48.9 13.0 113 235 0  600 800 0.10 0.10" $outfile1 \
        V "2 1 800 0.1 10.0 18.  20.0 10.0 18. 0.30 -30 30 0.15" $outfile2 \
     NONE 5200 1.1 0.4 1 | ./shdom90
endif
